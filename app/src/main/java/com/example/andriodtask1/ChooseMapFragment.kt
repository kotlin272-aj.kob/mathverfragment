package com.example.andriodtask1

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.andriodtask1.databinding.FragmentChooseMapBinding
import kotlinx.android.synthetic.main.fragment_choose_map.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ChooseMapFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChooseMapFragment : Fragment() {
    // TODO: Rename and change types of parameters


    private val REQUEST_SUM = 11
    private val score: Score = Score()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentChooseMapBinding>(
            inflater,
            R.layout.fragment_choose_map, container, false
        )
        setHasOptionsMenu(true)

        val args = ChooseMapFragmentArgs.fromBundle(requireArguments())
        score.correct = args.amoutCorrect
        score.wrong = args.amoutWrong

        binding.btnSumMenu.setOnClickListener {
            it.findNavController().navigate(ChooseMapFragmentDirections.actionChooseMapFragmentToMathSumFragment(score.correct,score.wrong))
        }
        binding.btnSubMenu.setOnClickListener {
            it.findNavController().navigate(ChooseMapFragmentDirections.actionChooseMapFragmentToMathSubFragment(score.correct,score.wrong))
        }
        binding.btnMutipleMenu.setOnClickListener {
            it.findNavController().navigate(ChooseMapFragmentDirections.actionChooseMapFragmentToMathMutiFragment(score.correct,score.wrong))
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            requireActivity().finish()
        }

        binding.score = score
        return binding.root
    }
//    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
//        super.onCreateOptionsMenu(menu, inflater)
//        inflater.inflate(R.menu.navdrawer_menu, menu)
//    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        return NavigationUI.
//        onNavDestinationSelected(item,requireView().findNavController())
//                || super.onOptionsItemSelected(item)
//    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == REQUEST_SUM) {
//            if (resultCode == Activity.RESULT_OK) {
//                txtAmountCorrectMain.text = data?.getStringExtra("correct")
//                txtAmountWrongMain.text = data?.getStringExtra("wrong")
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//                //TODO Handle Result Cancel
//            }
//        }
//    }
}
