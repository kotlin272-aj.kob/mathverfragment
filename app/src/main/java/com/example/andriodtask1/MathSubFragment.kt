package com.example.andriodtask1

import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.andriodtask1.databinding.FragmentMathSubBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MathSubFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MathSubFragment : Fragment() {
    // TODO: Rename and change types of parameters
//    private var param1: String? = null
//    private var param2: String? = null

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//        }
//    }

    lateinit var binding : FragmentMathSubBinding

    private var question: Question = Question()
    private var score: Score = Score()

    private var isAddScore :Boolean = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentMathSubBinding>(
            inflater,
            R.layout.fragment_math_sub, container, false
        )
        binding.score = score
        binding.question = question

        mainGame()
        getScore()
        setBtnBackToMain()

        return binding.root
    }

    private fun mainGame() {
        isAddScore = false
        question.createQuestionAndResultOfSub()
        question.createChoice()
        resetComponent()
        resetBackground()
        binding.invalidateAll()
    }



    private fun getScore(){
//        txtAmountCorrect.setText(intent.getStringExtra("correct"))
//        txtAmountWrong.setText(intent.getStringExtra("wrong"))
        val args = MathSubFragmentArgs.fromBundle(requireArguments())
        score.correct = args.amoutCorrect
        score.wrong = args.amoutWrong
    }

    private fun resetComponent(){
        question.msgResult = ""

        binding.apply {
            txtTime.text = ""
            txtNumber1.visibility = View.VISIBLE
            txtNumber2.visibility = View.VISIBLE
            txtOperator.visibility = View.VISIBLE

            btnAnswer1.setOnClickListener{
                onSelectAnswer(1,it)
            }
            btnAnswer2.setOnClickListener{
                onSelectAnswer(2,it)
            }
            btnAnswer3.setOnClickListener{
                onSelectAnswer(3,it)
            }
        }
        enableAllBtn()
    }
    private fun enableAllBtn(){
        binding.apply {
            btnAnswer1.setEnabled(true)
            btnAnswer2.setEnabled(true)
            btnAnswer3.setEnabled(true)
            btnBackSubToMain.setEnabled(true)
        }
    }
    private fun disableAllBtn(){
        binding.apply {
            btnAnswer1.setEnabled(false)
            btnAnswer2.setEnabled(false)
            btnAnswer3.setEnabled(false)
            btnBackSubToMain.setEnabled(false)
        }
    }

    private fun addAmountCorrect(){
        score.addCorrect()
        isAddScore = true
        binding.invalidateAll()
    }
    private fun addAmountWrong(){
        score.addWrong()
        isAddScore = true
        binding.invalidateAll()
    }
    private fun resetBackground(){
        binding.apply {
            txtNumberResult.setTextColor(Color.BLACK)
            btnAnswer1.setBackgroundColor( resources.getColor(R.color.buttonInit))
            btnAnswer2.setBackgroundColor( resources.getColor(R.color.buttonInit))
            btnAnswer3.setBackgroundColor( resources.getColor(R.color.buttonInit))
        }
    }
    private fun changeBackgroundWrong(btn: Button){
        binding.txtNumberResult.setTextColor(resources.getColor(R.color.wrong))
//        btn.setBackgroundColor( resources.getColor(R.color.wrong))
        btn.setBackgroundColor(Color.RED)
    }
    private fun changeBackgroundCorrect(btn: Button){
        binding.txtNumberResult.setTextColor(resources.getColor(R.color.correct))
//        btn.setBackgroundColor( resources.getColor(R.color.correct))
        btn.setBackgroundColor(Color.GREEN)
    }

    fun onSelectAnswer(choiceNumber:Int,view: View){
        val btn = (view as Button)
        var answer : Int = when(choiceNumber){
            1 -> question.choice1
            2 -> question.choice2
            3 -> question.choice3
            else -> 0
        }

        if(answer === question.numberResult ){
            question.msgResult = "ถูกต้อง แต่ไม่เพิ่มคะแนนนาจา"
            if(!isAddScore){
                question.msgResult = "ถูกต้อง"
                addAmountCorrect()
            }
            disableAllBtn()
            changeBackgroundCorrect(btn)
            contDownForNewGame()
        }else{
            question.msgResult = "${btn.text} ผิดจ้า เลือกใหม่นะ"
            if(!isAddScore){
                addAmountWrong()
            }
            changeBackgroundWrong(btn)
        }
        btn.setEnabled(false)
        binding.invalidateAll()
    }
    fun contDownForNewGame(){
        binding.apply {
            txtNumber1.visibility = View.INVISIBLE
            txtNumber2.visibility = View.INVISIBLE
            txtOperator.visibility = View.INVISIBLE
        }
        val timer = object: CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.txtTime.text = "ข้อต่อไปใน ${(millisUntilFinished/1000).toInt()} วิ"
            }
            override fun onFinish() {
                mainGame()
            }
        }
        timer.start()
    }

    fun setBtnBackToMain(){
//        val txtAmountCorrect = findViewById<TextView>(R.id.txtAmountCorrect)
//        val txtAmountWrong = findViewById<TextView>(R.id.txtAmountWrong)
//        val intent = Intent()
//        intent.putExtra("correct", txtAmountCorrect.text);
//        intent.putExtra("wrong", txtAmountWrong.text);
//        setResult(AppCompatActivity.RESULT_OK, intent);
//        finish();
        binding.btnBackSubToMain.setOnClickListener{
            it.findNavController().navigate(MathSubFragmentDirections.actionMathSubFragmentToChooseMapFragment(score.correct,score.wrong))
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            val navController = this@MathSubFragment.findNavController()
            navController.navigate(MathSubFragmentDirections.actionMathSubFragmentToChooseMapFragment(score.correct,score.wrong))
        }

    }


//    companion object {
//        /**
//         * Use this factory method to create a new instance of
//         * this fragment using the provided parameters.
//         *
//         * @param param1 Parameter 1.
//         * @param param2 Parameter 2.
//         * @return A new instance of fragment MathSubFragment.
//         */
//        // TODO: Rename and change types and number of parameters
//        @JvmStatic
//        fun newInstance(param1: String, param2: String) =
//            MathSubFragment().apply {
//                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
//                }
//            }
//    }
}